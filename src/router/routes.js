export default [
  {
    path: '',
    component: () => import('@/pages/Home.vue')
  },
  {
    path: 'register',
    component: () => import('@/pages/Register.vue')
  },
  {
    path: 'login',
    component: () => import('@/pages/Login.vue')
  },
  {
    path: '/info_user',
    component: () => import('@/pages/info/User.vue')
  },
  {
    path: '/info_organizer',
    component: () => import('@/pages/info/Organizer.vue')
  }
];
